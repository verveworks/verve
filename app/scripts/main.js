$(document).ready(function() {
	$('#pages').fullpage({
		anchors: ['home', 'soft-open', 'historia', 'aurorag', 'mindset', 'contato', 'clientes'],
		normalScrollElements: '.normal-scroll',
		scrollBar: true,
		navigation: true,
		navigationTooltips: ['Home', 'Soft Open', 'História', 'Aurora.ag', 'Mindset', 'Contato', 'Clientes'],
		navigationPosition: 'left',
		showActiveTooltip: true,
		bigSectionsDestination: 'top',
		fitToSection: false,
		afterLoad: function(anchorLink, index){
			var loadedSection = $(this);

			if(anchorLink != 'home') {
				$('.header .logo').fadeIn();
			} else {
				$('.header .logo').fadeOut();
			}

			$('#fp-nav, .fp-slidesNav').removeClass('white');

			switch(anchorLink) {
				case 'home':
					$('.header .logo').css({ display: 'none' });
				break;
				case 'contato':
					$('#fp-nav, .fp-slidesNav').addClass('white');
				break;
				case 'clientes':
					$('#fp-nav, .fp-slidesNav').addClass('white');
				break;
				case 'soft-open':
					$('#fp-nav, .fp-slidesNav').addClass('white');
				break;
				case 'mindset':
					$('#fp-nav, .fp-slidesNav').addClass('white');
				break;
			}
		}
	});

	var permitted = true;
	$('.form').on('submit', function () {
		var _this = this;
		var input_val = $(this).find('input').val();
		if(validateMail(input_val) == false){
			$('.msg').text('Por favor, preencha seu e-mail corretamente.').fadeIn();
			return false;
		} else {
			if(permitted == true){
				permitted = false;
				$.ajax({
					method: 'POST',
					url: 'http://verve.works/send-mail.php',
					data: { email: input_val },
					dataType: 'json',
					success: function (data) {
						$('.msg').text(data.msg).fadeIn();
						$(_this).find('input').val('');
						permitted = true;
					}
				});
			}
		}
		return false;
	});

	function validateMail(email) {
	    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	    return re.test(email);
	}

	window.sr = ScrollReveal({ 
		reset: true,
		duration: 500,
		delay: 200
	});
	sr.reveal('.home .logo');
	sr.reveal('.soft-open p');
	sr.reveal('.historia p');
	sr.reveal('.aurora h2');
	sr.reveal('.aurora li');
	sr.reveal('.mindset p');
	sr.reveal('.mindset .photo');
	sr.reveal('.contato .wrapper > p');
	sr.reveal('.contato input');
	sr.reveal('.contato button');
	sr.reveal('.foo li');

});