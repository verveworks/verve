<?php
if ( preg_match('/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/', $_POST['email'])) {
	$emailAddress = trim($_POST['email']);

	$to      = 'info@verve.works';
	$subject = 'E-mail vindo do site';
	$message = 'E-mail: '.$emailAddress;
	$headers = 'From: '.$emailAddress.'' . "\r\n" .
	    'Reply-To: info@verve.works' . "\r\n" .
	    'X-Mailer: PHP/' . phpversion();

	if(mail($to, $subject, $message, $headers)){
		$result = array('success' => 1, 'msg' => 'Mensagem enviada com sucesso.');
		echo json_encode($result);
		die;
	} else {
		$result = array('success' => 0, 'msg' => 'Não foi possível enviar a mensagem.');
		echo json_encode($result);
		die;
	}

} else {
	$result = array('success' => 0, 'msg' => 'Por favor, digite um e-mail válido.');
	echo json_encode($result);
	die;
}
?>